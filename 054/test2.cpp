#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

fstream plik("poker2.txt");

struct karta{
    int value;  //2,3,4,...,Ten (10), Jack(11) Queen(12) , King(13), Ace(14)
    int kolor;  // C=1 Diamonsds=2 Hearts=3, S=4,
    karta* next=NULL;//
};
// Wpisywanie kart
void dodajkarte(karta* (&fir), int wartosc,int kolor){
    karta* first=fir;
    if(first==NULL){
        fir=new karta;
        fir->value=wartosc;
        fir->kolor=kolor;
    }else{
        karta* last;
        last=first;
        while((first!=NULL)and(first->value<wartosc)){
            last=first;
            first=first->next;
        }
        if(first==NULL){
            first= new karta;
            first->value=wartosc;
            first->kolor=kolor;
            last->next=first;
            first= NULL;
        }else if(first==fir){
            karta* tmp = new karta;
            tmp->value=wartosc;
            tmp->kolor=kolor;
            tmp->next=first;
            fir=tmp;
            tmp=NULL;
            
        
        }
        else{
            karta* tmp =new karta;
            tmp->value=wartosc;
            tmp->kolor=kolor;
            tmp->next=first;
            last->next=tmp;
            tmp=NULL;
        }
        
    }
}


void wypiszreke(karta* first){
    while(first!=NULL){
        cout<<"("<<first->value<<","<<first->kolor<<")";
        first=first->next;
    }
    
}

void wyczyscreke(karta* &first){
    if(first->next==NULL)delete first;
    else{
        wyczyscreke(first->next);
        delete first;
    }
    first=NULL;
    
}

void wartosckarty(string pole, int &x,int &y){
    if(pole[0]=='T')x=10;
    else if(pole[0]=='J')x=11;
    else if(pole[0]=='Q')x=12;
    else if(pole[0]=='K')x=13;
    else if(pole[0]=='A')x=14;
    else{
        x=(int)pole[0] -48;
    }
    if(pole[1]=='C')y=1;
    else if(pole[1]=='D')y=2;
    else if(pole[1]=='H')y=3;
    else y=4;
    
}

void wczytajreke(karta* (&reka)){
    string pole;
    int x,y;
    for(int i=0;i<5;i++){
        plik>>pole;
        wartosckarty(pole,x,y);
        dodajkarte(reka,x,y);
    }
}

void discardHighest(karta* (&reka)){
    karta* first ,*second,*last;
    if(reka->next==NULL){
        delete reka;
        reka=NULL;
    }else{
        first=reka;
        second=first;
        while(first!=NULL){
            last=second;
            second=first;
            first=first->next;
        }
        delete second;
        last->next=NULL;
    }
}

// Liczenie wartosci
bool czy_suit(karta* reka){// Sprawdza, czy ten sam kolor. OK
    bool suit =true;
    int kolor=reka->kolor;
    while((reka!=NULL)and(suit)){
        if(kolor!=reka->kolor)suit=false;
        reka=reka->next;
    }
    return suit;
    
}


int Straight(karta* reka){ // zwraca Wartosc najw karty w Straightcie, jesli nie ma - 0 . OK
    bool flag=true;
    int wartosc=reka->value;
    reka=reka->next;
    while((reka!=NULL)and flag){
        if(reka->value==wartosc+1)wartosc++;
        else flag=false;
        reka=reka->next;
    }
    if(flag)return wartosc;
    else return 0;
}

int ileTychSamych(karta* reka){// moze zwracac 144- asy (14) cztery (4) //  23- tzy dwojki  .OK
    int maxlen=0;
    int maxlenCardValue,CardValue,len;
    CardValue=reka->value;
    len=1;
    reka=reka->next;
    while(reka!=NULL){
        if(reka->value==CardValue)len++;
        else{
            if(len>maxlen){
                maxlenCardValue=CardValue;
                maxlen=len;
            }else if(len==maxlen){
                if(CardValue>maxlenCardValue)maxlenCardValue=CardValue;
            }
            CardValue=reka->value;
            len=1;
        }
        reka=reka->next;
    }
    if(len>maxlen){
        maxlenCardValue=CardValue;
        maxlen=len;
    }else if(len==maxlen){
        if(CardValue>maxlenCardValue)maxlenCardValue=CardValue;
    }
    return maxlenCardValue*10+maxlen;
}

int HighestCard(karta* reka){ //OK
    int wartosc;
    while(reka!=NULL){
        wartosc=reka->value;
        reka=reka->next;
    }
    return wartosc;
}

bool twoPairs(karta* reka){// DO NAPISANA
    int card,s[14],j;
	bool para,drugaPara;
	for(int i=0;i<14;i++)s[i]=0;
	
    while(reka!=NULL){
        s[reka->value]++;
		reka=reka->next;
    }
    para=drugaPara=false;
    for(int i=2;i<=14;i++){
        if((s[i]==2)and para)drugaPara=true;
        if(s[i]==2)para=true;
    }
    return (drugaPara);
}
bool wygrywatwoPairs(karta *first,karta *second){
    int card,f[14],s[14],j;
	for(int i=0;i<14;i++)f[i]=s[i]=0;
	karta*reka1=first;
	karta *reka2=second;
    while(reka1!=NULL){
        f[reka1->value]++;
        s[reka2->value]++;
		reka1=reka1->next;
		reka2=reka2->next;
    }
    j=14;
    while(j>=2){// sprawdzam ktora ma najdalej pare na prawo ( najwieksza wartosc) jesli tak samo - break
        if(f[j]==2 and s[j]!=2)return true;
        else if(s[j]==3 and f[j]!=2)return false;
        else if(f[j]==3 and s[j]==2)break;
    }
    j=2;
    while(j<=14){//  sprawdzam ktora ma pare najblizej 2ki. ta przegrywa
        if(f[j]==2 and s[j]!=2)return false;
        else if(s[j]==2 and f[j]!=2)return true;
        else if(f[j]==2 and s[j]==2)break;
    }
    cout<< "twoPairs error";
    
}

bool FullHouse(karta* reka){// DO NAPISANA
    int card,s[14],j;
	bool para,trojka;
	for(int i=0;i<14;i++)s[i]=0;
	
    while(reka!=NULL){
        s[reka->value]++;
		reka=reka->next;
    }
    para=trojka=false;
    for(int i=2;i<=14;i++){
        if(s[i]==2)para=true;
        if(s[i]==3)trojka=true;
    }
    return (para and trojka);
}

bool wygrywaFullHouse(karta *first,karta *second){
    int card,f[14],s[14],j;
	for(int i=0;i<14;i++)f[i]=s[i]=0;
	karta*reka1=first;
	karta *reka2=second;
    while(reka1!=NULL){
        f[reka1->value]++;
        s[reka2->value]++;
		reka1=reka1->next;
		reka2=reka2->next;
    }
    j=14;
    while(j>=2){
        if(f[j]==3 and s[j]!=3)return true;
        else if(s[j]==3 and f[j]!=3)return false;
        else if(f[j]==3 and s[j]==3)break;
    }
    j=14;
    while(j>=2){
        if(f[j]==2 and s[j]!=2)return true;
        else if(s[j]==2 and f[j]!=2)return false;
        else if(f[j]==2 and s[j]==2)break;
    }
    cout<< "FullHouse error";
    
}

int handValue(karta* reka){
        /*
    2-14    High Card: Highest value card.
    15-27     One Pair: Two cards of the same value.
    28      Two Pairs: Two different pairs.                         //dla two pairs moge na dole sprawdzac ktor par wieksza
    29-41      Three of a Kind: Three cards of the same value.         ///gorzej, jesli bedzie para wieksza taka samass
    42-54 Straight: All cards are consecutive values.             
    55-67      Flush: All cards of the same suit.
    68      Full House: Three of a kind and a pair.                     //osobne porownanie na dole jak w parze dwoj
    69-81      Four of a Kind: Four cards of the same value.
    82-94      Straight Flush: All cards are consecutive values of same suit.
    95      Royal Flush: Ten, Jack, Queen, King, Ace, in same suit./*/   ///chyba bez tego, to przeciez to samo co 94
    bool suit=czy_suit(reka);
    int ile_tych_samych;
    if(suit){           ///                FUNKCJE DO NAPISANIA   Para szostek wieksza niz para 2jek... itd... pff
        if(Straight(reka)>0)return 80+Straight(reka);
    }
    ile_tych_samych=ileTychSamych(reka);
    if(ile_tych_samych%10==4)return int(ile_tych_samych/10 + 67);
    if(ile_tych_samych%10==3){
        if(FullHouse(reka))return 68;
    }
    if(suit)return 53+HighestCard(reka);
    if(Straight(reka)>0)return 40+Straight(reka);
    if(ile_tych_samych%10==3)return int(27 +ile_tych_samych/10);
    if(ile_tych_samych%10==2){
        if(twoPairs(reka))return 28;
        else return int(13+ ile_tych_samych/10);
    }
    return (HighestCard(reka));
}
int main(){
    karta* first, *second;
    int fValue,sValue,licznik,x;
    bool foundHighest;
    licznik=0;
    while(!plik.eof()){
  
        wczytajreke(second);
        cout<<handValue(second)<<" ";
        wczytajreke(first);
        cout<<handValue(first)<<endl;
    }
    cout<<licznik;
}
