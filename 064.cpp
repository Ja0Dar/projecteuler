#include <iostream>
#include <cmath>
using namespace std;

const int N=10000;
struct wyr{  /// Wolimy miec wartosci w dgt, a nei mult
    int mult=1;     ///Gdy sq=false mult=1, dgt=wartosc
    bool sq=false;
    int dgt;
};
struct frac{
    wyr l;
    wyr m;
    
};

bool rowneWyr(wyr a, wyr b){
    return a.sq==b.sq and a.dgt==b.dgt and a.mult==b.mult;
}

bool rowneFrac(frac a, frac b){
    return rowneWyr(a.l,b.l) and rowneWyr(a.m,b.m);
}

void wypisz(frac a,int x);

int NWD(int a,int b){
    int tmp;
    while(a>0){
        tmp=a;
        a=b%a;
        b=tmp;
        
    }
    return b;
}


void odwroc(frac &a){
    wyr tmp=a.l;
    a.l=a.m;
    a.m=tmp;
}



void uniewymiernij(frac &a, int x){
    if(a.m.sq and !a.l.sq){
        int nwd;
        int tmp;
        tmp=a.m.dgt;
        a.m.dgt=x-tmp*tmp;
        a.l.mult=a.l.dgt;
        nwd=NWD(a.l.mult,a.m.dgt);
        a.l.mult/=nwd;
        a.m.dgt/=nwd;
        a.l.sq=true;
        a.m.sq=false;
        a.m.mult=1;
        a.l.dgt=tmp*-1;
    }else {
        cout<<"uniewymierniasz bez niewymiernosci";
        wypisz(a,x);
    }
    
}

/*int*/void wylaczPrzedUlamek(frac &a, int a0){
    //a0= sqrt(x)
    //if(a.l.mult!=1)cout<<"multiplier w wylaczPrzedUlamek";
    int wynik=(a0+a.l.dgt)/a.m.dgt;
    a.l.dgt=a.l.dgt-wynik*a.m.dgt;
    //return wynik;
}

void wypisz(frac a,int x){
    cout<<a.l.mult<<"*(";
    if(a.l.sq)cout<< "sqrt("<<x<<")";
    cout<< " + "<<a.l.dgt<<")"<<endl<<"_________________"<<endl<<a.m.mult<<"(";
    if(a.m.sq)cout<< "sqrt("<<x<<")";
    cout<< " + "<<a.m.dgt<<")"<<endl;
}


int main(){
    int x,a0,periodLen,licznik;
    frac a,reszta;
    licznik=0;
    
    
    
    for(int x=1;x<=N;x++){
        a0=int(sqrt(x));
        if(a0*a0!=x){
            a.l.sq=true;
            a.l.dgt=a0*-1;
            a.l.mult=a.m.mult=a.m.dgt=1;
            a.m.sq=false;
            reszta=a;
            
            
            periodLen=0;
            do{
                odwroc(a);
                uniewymiernij(a,x);
                
                wylaczPrzedUlamek(a,a0);
                periodLen++;
            }
            while(!rowneFrac(a,reszta));
            
            if(periodLen%2==1)licznik++;
        }
    }
    cout<<licznik;
}