#include <iostream>
#include <cmath>
#include <climits>
using namespace std;


/*Prime pair sets
Problem 60
The primes 3, 7, 109, and 673, are quite remarkable. 
By taking any two primes and concatenating them in any order the result will always be prime.
For example, taking 7 and 109, both 7109 and 1097 are prime. The sum of these four primes, 792, 
represents the lowest sum for a set of four primes with this property.

Find the lowest sum for a set of five primes for which any two primes concatenate to produce another prime.


liczby te musza przystawac mod 3 (wyjatek - 3)

Wyszukuje dobry wynik w 20s, ale daje ciala szukajac reszty.

*/
const int N=600;
const int JAK_DLUGI=5;

struct node{
    int val;
    node* next =NULL;
};

int minsum=INT_MAX;

void appendNode(node *(&fir),int value){
    if(fir==NULL){
        fir= new node;
        fir->val=value;
    }else{
        node *first=fir;
        while(first->next!=NULL){
            first=first->next;
        }
        first->next=new node;
        first->next->val=value;
    }
}

void delLastNode(node *(&fir)){
    if(fir!=NULL){
        if(fir->next==NULL){
            delete fir;
            fir=NULL;
        }else{
            node *first=fir;
            node *last=first;
            while(first->next!=NULL){
                last=first;
                first=first->next;
            }
            delete first;
            last->next=NULL;
        }
    }
}


void wypisz(node* first){
    while(first!=NULL){
        cout<<first->val<<endl;
        first=first->next;
    }
}

bool is_prime(int liczba){
    if(liczba==2) return true;
    else if(liczba%2==0)return false;
    else if(liczba<2)return false;
    else{

        int factor=3;
        while(factor*factor<=liczba){
            if(liczba%factor==0)return false;
            factor=factor+2;
        }
        return true;
        
    }
}

int sumaLancucha(node *first){
    int suma =0;
    while(first!=NULL){
        suma+=first->val;
        first=first->next;
    }
    return suma;
}

void sprawdz(node* (&first),int len,int pr[N],int prI){//sprawdz(first,1,pr1,-1) //first!=Null first->val okresolne
    //first != null- zaimplementuj?
    bool allPrimes;
    node* l;
    int suma=sumaLancucha(first);
    if(len==JAK_DLUGI){
        if(suma<minsum)minsum=suma;
        cout<<"suma= "<<suma<<endl;
        wypisz(first);
        cout<<endl;
        
    }else if(prI<N){
        int n,liczba;
        for(int j=prI+1;j<N;j++){
            if(suma+(5-len)*pr[j]<minsum){
                allPrimes= first!=NULL;
                l=first;
                if(allPrimes){
                    while(l!=NULL){
                        liczba=pr[j];
                        n=l->val;
                        while(n>0){
                            liczba*=10;
                            n=n/10;
                        }
                        liczba+=l->val;
                        if(!is_prime(liczba))allPrimes=false;
                        
                        liczba=l->val;
                        n=pr[j];
                        while(n>0){
                            liczba*=10;
                            n=n/10;
                        }
                        liczba+=pr[j];
                        if(!is_prime(liczba))allPrimes=false;
                        
                        
                        l=l->next;
                    }
                }
                if(allPrimes){
                    appendNode(first,pr[j]);
                    sprawdz(first,len+1,pr,j);
                    delLastNode(first);
                }
            }       
            
            
            
        }
    }
}


int main(){
    int pr1[N],pr2[N],maxsum; // pr2 - pierwsze dajace reszte z dzielenia przez 3 rowna 2 alalogia - pr1 <=>prime%3=1
    int m=0;
    int n=0;
    int k=6;
    
    while((n<N)or(m<N)){
        if(is_prime(k-1)and (n<N)){
            pr2[n]=k-1;
            n++;
        }
        if(is_prime(k+1)and (m<N)){
            pr1[m]=k+1;
            m++;
        }
        k+=6;
    }
    
    node *first;
    first=new node;
    first->val=3;
    sprawdz(first,1,pr1,-1);
    sprawdz(first,1,pr2,-1);
    
    for(int i=0;i<N;i++){
        if((pr1[i]*5>minsum)and(pr2[i]*5>minsum))break;
        first->val=pr1[i];
        sprawdz(first,1,pr1,i);
        first->val=pr2[i];
        sprawdz(first,1,pr2,i);
    }
    
    cout<<endl<<endl<<minsum<<endl<<INT_MAX;
    
    
    
    
}