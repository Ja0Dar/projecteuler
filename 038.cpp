#include <iostream>
#include <cmath>
using namespace std;
const int  N=1000000000;

int main(){
    bool digits[10],brak_powtorzen;
    int pow10[8],k,n,tab_i,multiplier,digitNumber,j,digit;
    long long int pandigital,pandigital_max;
    
    digits[0]=true; // nie do wykorzystania
    //for (int i=1;i<10;i++)digits[i]=false;
    
    pow10[0]=1;
    //for(int i=0;i<9;i++)tab[i]=0;
    for(int i=1;i<8;i++)pow10[i]=pow10[i-1]*10;
    
    k=2;
    pandigital_max=0;
    while(k<10000){
        
        for (int i=1;i<10;i++)digits[i]=false;
        brak_powtorzen=true;
        multiplier=1;
        tab_i=0;
        while(brak_powtorzen and (tab_i<9)){
            n=k*multiplier;
            digitNumber=int(log10(n))+1;
            
            if(multiplier==1)pandigital=n;
            else pandigital=pandigital*pow10[int(log10(n)+1)]+n;
            
            while(n>0){
                digit=n%10;
                n=n/10;
                if(!digits[digit]){//jesli nie byla uzyta
                    digits[digit]=true;
                    tab_i++;
                    j++;
                }else brak_powtorzen=false;
            }
            multiplier++;
        }
        if(brak_powtorzen){
            cout<<k<<"    "<<pandigital;
            if(pandigital_max<pandigital)pandigital_max=pandigital;
            //for (int i=0;i<9;i++)cout<<tab[i];
            cout<<endl;
        }
        k++;
    }
    cout<<"MAX "<<pandigital_max;
}