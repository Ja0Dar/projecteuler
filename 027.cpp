#include<iostream>
#include <cmath>
using namespace std;

const int N=1000;
 
bool is_prime(int a){
    int factor=3;
    if(a<=0)return false;
    if(a==1)return false;
    if(a==2)return true;
    if(a%2==0)return false;
    while(factor*factor<=abs(a)){
        if(a%factor==0)return false;
        factor=factor+2;
    };
    return true;
}

int main(){
    int n,a,a0,b0,b,mostPrime,amountOfPrimes,value;
    mostPrime=0;
    for(a=-1*N;a<=N;a++){
        for(b=0;b<=N;b++){
            if(is_prime(b)){
                amountOfPrimes=1;
                n=1;
                value=n*n+a*n+b;
                while(is_prime(value)){
                    amountOfPrimes=amountOfPrimes+1;
                    n=n+1;
                    value=n*n+a*n+b;
                };
                if(amountOfPrimes>mostPrime){
                    cout<<"n*n+("<<a<<"*n)+ "<<b<<" ma "<<amountOfPrimes<<endl;
                    a0=a;
                    b0=b;
                    mostPrime=amountOfPrimes;
                };
            };
            
        };
    };
    cout<<a0*b0;
}
