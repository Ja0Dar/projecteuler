#include <iostream>
#include <cmath>
using namespace std;

const int N=15;

bool is_prime(int liczba){
    if(liczba==2) return true;
    else if(liczba%2==0)return false;
    else if(liczba<2)return false;
    else{

        int factor=3;
        while(factor*factor<=liczba){
            if(liczba%factor==0)return false;
            factor=factor+2;
        }
        return true;
        
    }
}


int main(){
    bool all_primes;
   int digitNumber,j;
   long long int n,k,suma=0;
   int pow10[N];
   pow10[0]=1;
   for(int i=1;i<=N;i++)pow10[i]=pow10[i-1]*10;
   
   
   k=11;
   while(k<1000000){
       digitNumber=log10(k)+1;
       if(is_prime(k)){
           //digitNumber=log10(k)+1;
           
           //najpierw odcinam cyfry z prawej
           n=k/10;
           all_primes=true;
           while((n>0)and all_primes){
               if(!(is_prime(n)))all_primes=false;
               n=n/10;
           }
           
           //teraz z lewej bede odcinal
           n=k;
           j=digitNumber-1;
           while((j>=0)and all_primes){
               if(!(is_prime(n)))all_primes=false;
               n=n%pow10[j];
               j--;
           }
           if(all_primes){
               cout<<k<<endl;
               suma+=k;
           }
       }
       k=k+2;
   }
   cout<<"suma:"<<suma<<endl;
}