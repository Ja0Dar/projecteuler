nLychrel=[]
found=False
def reverseNumber(number):
    wynik=0
    while(number>0):
        wynik=wynik*10+number%10
        number//=10
    return wynik

def isPalindrome(number):
    a=number
    wynik=0
    while a>0:
        wynik=wynik*10+a%10
        a//=10
    return wynik==number

def checkLychrel(number,iteration):
    #print(number)
    global found
    if iteration==51:
        return True
    else:
        if number in nLychrel:
            return False
        else:
            a=reverseNumber(number)
            if a==number:
                return False
            else:
                if not checkLychrel(number+a,iteration+1):
                    nLychrel.append(number)
                    return False
                else:
                    return True
licznik=0
a=10000
while a!=0:
    if checkLychrel(a+reverseNumber(a),1):
        licznik+=1
    a-=1
print(licznik)
    
