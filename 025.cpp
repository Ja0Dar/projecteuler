#include <iostream>
using namespace std;

const int N=1000;

int dodaj(int a[],int aI, int b[],int bI,int (&c) [N],int &cI ){ //c - wynik
    int p,tmp;//range - maksymalna cyfra nie zero, p - w pamieci w dodawaniu
    for(int i=0;i<N;i++)c[i]=0;
    
    if(bI>aI)cI=bI+1;
    else cI=aI+1;
    p=0;
    for (int i=0;i<=cI;i++){
        tmp=a[i]+b[i]+p;
        c[i]=tmp%10;
        p=tmp/10;
    };
    if(c[cI]==0)cI=cI-1;
}


void przypisz(int(&m)[N],int &mI,int (&n)[N],int &nI){
    for (int i=0;i<=nI;i++){
        m[i]=n[i];
    };
    mI=nI;
}

int main(){
 int pprev[N];
 int prev[N];
 int nowy[N];
 int pprevI,prevI,nowyI,licznik; //pprev - przedostatni wyraz ciagu prev - ostatni, nowy - nowy
    // nowyI itp - indexy najwi�kszego nie o
    for (int i=0;i<N;i++){
        pprev[i]=0;
        prev[i]=0;
        nowy[i]=0;
    };
    pprev[0]=prev[0]=1; //liczby beda czytane od ty�u.- prev[0] to cyfra jednostek
    pprevI=prevI=0;
    licznik=2;
    while (nowyI<1000-1){
        dodaj(pprev,pprevI,prev,prevI,nowy,nowyI);
        przypisz(pprev,pprevI,prev,prevI);//pprev=prev;
        przypisz(prev,prevI,nowy,nowyI);//prev=nowy;
        licznik=licznik+1;
    };
    cout<<licznik;
}
    
