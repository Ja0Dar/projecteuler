#include<iostream>
using namespace std;

const int N=1000;
const int K=1000;

void pomnoz(int tab[N],int n,int wynik[N]){
    int tmp,p=0;
    for(int i=N-1;i>=0;i--){
        tmp=tab[i]*n+p;
        p=tmp/10;
        wynik[i]=tmp%10;
    }
}

int zeruj(int tab[N]){
    for(int i=0;i<N;i++)tab[i]=0;
}


void dodaj(int tab[N], int n){
    int tmp,i=N-1;
    while((i>=0)and (n!=0)){
        tmp=tab[N]+n;
        tab[N]=tmp%10;
        n=tmp/10;
        i--;
        
    }
}
void dodajTab(int t1[N],int t2[N],int wynik[N]){
    int tmp;
    int p=0;
    for(int j=N-1;j>=0;j--){
        tmp=t1[j]+t2[j]+p;
        p=tmp/10;
        wynik[j]=tmp%10;
        
    }
}

void reverse(int *(&a),int*(&b)){
    int*c;
    c=a;
    a=b;
    b=c;
}

void wypisz(int tab[N]){
    bool allZeroes=true;
    int i=0;
    while(allZeroes){
        if(tab[i]==0)i++;
        else allZeroes=false;
        
    }
    while(i!=N){
        cout<<tab[i];
        i++;
    }
}
int countDigits(int tab[N]){
    bool noZero=true;
    int i=0;
    while(noZero){
        if(tab[i]==0)i++;
        else noZero=false;
    }
    return N-i;
}
int main(){
    int licznik;
    int *l,*m;
    l=new int[N];
    m=new int[N];
    zeruj(l);
    zeruj(m);
    *(l+N-1)=1;
    *(m+N-1)=1;
    for(int i=0;i<K;i++){
       dodajTab(l,m,l);//1+l/m
        reverse(l,m);// 1/ulamek
        dodajTab(l,m,l);
       if(i%1000==0){
            wypisz(l);
            cout<<"/";
            wypisz(m);
            cout<<endl<<endl;
            cout<<countDigits(l)<<" "<<countDigits(m);
            cout<<endl<<"i="<<i<<endl;
        }
        if(countDigits(l)>countDigits(m))licznik++;
        
    }
    cout<<licznik;
    
    
}
