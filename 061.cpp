#include <iostream>
#include <cmath>
using namespace std;


struct node{
    int val;
    node* next =NULL;
};

int sumaLancucha(node *first){
    int suma =0;
    while(first!=NULL){
        suma+=first->val;
        first=first->next;
    }
    return suma;
}
void appendNode(node *(&fir),int value){
    if(fir==NULL){
        fir= new node;
        fir->val=value;
    }else{
        node *first=fir;
        while(first->next!=NULL){
            first=first->next;
        }
        first->next=new node;
        first->next->val=value;
    }
}

void delLastNode(node *(&fir)){
    if(fir!=NULL){
        if(fir->next==NULL){
            delete fir;
            fir=NULL;
        }else{
            node *first=fir;
            node *last=first;
            while(first->next!=NULL){
                last=first;
                first=first->next;
            }
            delete first;
            last->next=NULL;
        }
    }
}


void wypisz(node* first){
    while(first!=NULL){
        cout<<first->val<<endl;
        first=first->next;
    }
}


void szukaj(node *first,bool prop[5]){
    int licz=1,n,brakujaca;
    node* last;
    last=first;
    while(last->next!=NULL){
        last=last->next;
        licz++;
        
    }
    if(licz==5){///sprawdzac ma  liczbe bedaca koncowka last + poczatek first
        bool doWypisania=false;
        int doSpr=(last->val%100)*100+first->val/100;
        for(int i=0;i<5;i++)if(prop[i]==false)brakujaca=i;
        switch(brakujaca){
            case 0:
                n=(sqrt(1+8*doSpr)-1)/2;
                if(n*(n+1)/2==doSpr)doWypisania=true;
                break;
            case 1:
                n=(1+sqrt(24*doSpr+1))/6;
                if(n*(3*n-1)/2==doSpr)doWypisania=true;
                break;
            case 2:
                n=(1+sqrt(6*doSpr+1))/4;
                if(n*(2*n-1)==doSpr)doWypisania=true;
                break;
            case 3:
                n=(3+sqrt(40*doSpr+9))/10;
                if(n*(5*n-3)/2==doSpr)doWypisania=true;
                break;
            case 4:
                n=(2+sqrt(12*doSpr+4))/6;
                if(n*(3*n-2)==doSpr)doWypisania=true;
                break;
            default:
                cout<<"Brakujaca switch error";
                cin.ignore();
        }
        if(doWypisania){
            appendNode(first,doSpr);
            wypisz(first);
            cout<<endl<<"suma: "<<sumaLancucha(first)<<endl;
            delLastNode(first);
        }
    }else{
        int generowana;
        int poczatek=last->val%100;
        int n0[]={45,26,23,21,19};
        for(int i=0;i<5;i++){//triangle, Penta, hexa ,hepta i octagonal
            if(!prop[i]){//nie bylo tej wlasnoscic
            n=10;//n0[i];
                switch(i){
                    case 0://triangle
                        generowana=n*(n+1)/2;
                        break;
                    case 1://Pentagonal
                        generowana=n*(3*n-1)/2;
                        break;
                    case 2://hexagonal
                        generowana=n*(2*n-1);
                        break;
                    case 3://heptagonal
                        generowana=n*(5*n-3)/2;
                        break;
                    case 4://octagonal
                        generowana=n*(3*n-2);
                        break;
                    
                }
                while(generowana/100<=poczatek){//nie optymalne?
                    if((generowana/100==poczatek)and ((generowana/10)%10!=0)){
                        //cout<<"x";
                        appendNode(first,generowana);
                        /*wypisz(first);
                        cout<<endl;
                        cout<<i<<endl;*/
                        prop[i]=true;
                        szukaj(first,prop);
                        prop[i]=false;
                        delLastNode(first);
                            
                    }
                    n++;
                    switch(i){
                        case 0://triangle
                            generowana=n*(n+1)/2;
                            break;
                        case 1://Pentagonal
                            generowana=n*(3*n-1)/2;
                            break;
                        case 2://hexagonal
                            generowana=n*(2*n-1);
                            break;
                        case 3://heptagonal
                            generowana=n*(5*n-3)/2;
                            break;
                        case 4://octagonal
                            generowana=n*(3*n-2);
                            break;
                        
                    }
                        
                }
            }
        }
    }
    
}

int main(){
    node *first;
    first=new node;
    int n=1;
    first->val=1;
    bool wlasnosci[5];//triangle, pentagonal hexagonal heptagonal, octagonal
    for(int i=0;i<5;i++)wlasnosci[i]=false;
    while(first->val<10000){
        first->val=n*n;
        if((first->val>999)and((first->val/10)%10!=0))szukaj(first,wlasnosci);
        n++;
    }
    
}