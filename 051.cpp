#include<iostream>
#include <cmath>


using namespace std;

const int N=8;
void inkrementuj(int tab[],bool &koniec){
    int i=N-1;
    while((tab[i]==9)and i>=0)i--;
    tab[i]+=1;
    for(int k=i+1;k<N;k++)tab[k]=0;
}

int fisrstNot0(int tab[]){
    int fN0=N-1;
    
    while(tab[fN0]!=0){
        fN0--;
    }
    return fN0+1;
    
}

int tabToInt(int tab[N]){
    int liczba=0;
    for(int i=0;i<N;i++){
        liczba=liczba*10+tab[i];
    }
    return liczba;
}


bool is_prime(int liczba){
    if(liczba==2) return true;
    else if(liczba%2==0)return false;
    else if(liczba<2)return false;
    else{

        int factor=3;
        while(factor*factor<=liczba){
            if(liczba%factor==0)return false;
            factor=factor+2;
        }
        return true;
        
    }
}

int main(){
    
    int liczba[N],mask,n,poczatek,licznik,wynik;
    bool koniec, found;
    for(int i=0;i<N;i++)liczba[i]=0;
    
   
    
    koniec=found=false;
    while(!koniec and !found){
        inkrementuj(liczba,koniec);
        poczatek=fisrstNot0(liczba);
        mask=pow(2,N-poczatek);
        for(int j=1;j<mask;j++){
            licznik=0;
            for(int input=0;input<10;input++){  
                if(!(j%2==1 and input==0)){ //gdy wstawiam na 1 cyfre i input==0 to mam liczby o mniejszej ilosci cyfr
                    n=j;
                    wynik=0;
                    for(int i=poczatek;i<N;i++){  //tutaj tworze liczbe przez wpisywanie cyfr do luk
                        if(n%2==1)wynik=wynik*10+input;
                        else wynik=wynik*10+liczba[i];
                        n=n/2;
                        }
                    if(is_prime(wynik))licznik++;
                }
            }
            if(licznik>=8){
                cout<<tabToInt(liczba)<<endl;
                found=true;
            }
        }
    }
}