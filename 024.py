
def odwroc(l): #funkcja do odwracania tablicy cyfr
    llen=len(l)
    a=[]
    for i in range(0,len(l)):
        a.append(l[llen-i-1])
    return a

#funkcja generujaca permutacje wieksza od poprzedniej. w parametrze musi miec podany index pierwszego pola, które ma sie zmienic
#:
def zwieksz(tab,ind):  
    warPola=tab[ind]# oryginalna wartosc zwiekszanego pola, ind - index zwiekszanego pola
    czyJestCyfra=[False,False,False,False,False,False,False,False,False,False]
    #sprawdzam jakie sa cyfry do ind
    for i in range(0,ind+1):
        czyJestCyfra[tab[i]]=True
    #podstawiam pod tab[ind] najblizsza cyfre wieksza od tab[ind], nie uzyta przed ind:
    i=0
    while czyJestCyfra[i] or i<= tab[ind]:
        i=i+1
    tab[ind]=i

    #teraz bede dopisywal cyfry od ind+1 do konca
        
    czyJestCyfra=[False,False,False,False,False,False,False,False,False,False]  #true- cyfra bedzie dostepna do dopisania
    czyJestCyfra[warPola]=True #ustawiam pierwotna wartosc tab[ind] na dostepna
    


    #ustawiam wartosc cyfr po ind na dostepne
    for i in range(ind+1,len(tab)):
        czyJestCyfra[tab[i]]=True
    czyJestCyfra[tab[ind]]=False # zeby prog nie dopisal cyfry ktora juz uzylem na tab[ind]
        
    j=ind+1 #index pol na ktore bede rosnaco dopisywal dostepne cyfry
    for i in range(0,10):
        if czyJestCyfra[i]:
            tab[j]=i
            j=j+1 
    
    
l=[0,1,2,3,4,5,6,7,8,9]
#l=[1,2,3,4]
odwrot=odwroc(l)
maxI=len(l)-2 
i=maxI #i  wskazuje ktora cyfra bedzie pierwsza od lewej zmieniajaca sie ( ta cyfra sie zwiekszy)
nr=1 # do liczenia ktora w kolei liczba jest generowana
while l!= odwrot:
    
    if l[i]<l[i+1]:
        zwieksz(l,i)
        nr=nr+1
        i=maxI
    else:
        i=i-1 # bede zajmowal sie cyfra bardziej na lewo
    if nr==1000000:
        print(l)
    
    
