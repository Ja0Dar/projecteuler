#include <iostream>
#include <cmath>
using namespace std;

const int N=10000000;

//bool teSameCyfry(int a,int b);


bool teSameCyfry(int a,int b){
    int cyfryA[10];
    int cyfryB[10];
    for (int i=0;i<10;i++)cyfryB[i]=cyfryA[i]=0;
    while(a>0){
        cyfryA[a%10]++;
        a=a/10;
    }
    while(b>0){
        cyfryB[b%10]++;
        b=b/10;
    }
    bool teSame=true;
    int i=0;
    while(teSame and i<10){
        if(cyfryB[i]!=cyfryA[i])teSame=false;
        i++;
    }
    if(teSame)return true;
    else return false;
}



int biggerpow10(int liczba){
    int pow10=1;
    while(liczba>0){
        liczba/=10;
        pow10*=10;
    }
    return pow10;
}


int main(){
   int j,i=1;
   int liczba;
   bool found =false;
   while((i<N)and(!found)){
       //liczba=biggerpow10(i)+i;  to o dziwo zwalnia tylko.
       liczba=i;
       if(teSameCyfry(liczba,liczba*2)){
           found=true;
           j=3;
           while((j<=6)and(found)){
               if(!teSameCyfry(liczba,liczba*j))found=false;
               j++;
           }
                       
       }
       i++;
   }
   cout<<liczba;
}