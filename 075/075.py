##m>n
##a=m^2-m*n
##b=2*n*m
##c=m*2+n*2
##l=2*m(m+n)
##primitywne <-> (m-n)%2=1 i nwd(m,n)==1
L=15000

def NWD(a,b):
    while(a!=0):
        tmp=a
        a=b%a
        b=tmp
    return b

#t=[]
##for i in range(0,L+1):
##    t.append(0)
li={}
maxM=L//2#?
for m in range(1,maxM):
    for n in range(1,m):
        if NWD(n,m)!=1 or (m-n)%2==0:
            continue
##        msq=m*m
##        nsq=n*n
##        a=msq-nsq
##        b=m*n*2
##        c=msq+nsq
##        
        l=2*m*(n+m)
        for i in range(l,L+1,l):
            if i in li:
                li[i]+=1
            else:
                li[i]=1
suma=0
for i in li:
    if li[i]==1:
        suma+=1

print(suma)
