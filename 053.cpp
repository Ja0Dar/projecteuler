#include <iostream>
using namespace std;
const int N=1000000;
long long int kombinacja(int n, int k){
    if(k>n-k)k=n-k;
    long long int licznik;
    licznik=1;
    for(int i=n-k+1;i<=n;i++)licznik*=i;
    
    for(int i=1;i<=k;i++)licznik=licznik/i;
    return licznik;
}

int main(){
    int k,l=0;
    
    for(int n=1;n<=100;n++){
        k=0;
        while((kombinacja(n,k)<=N)and k<n)k++;
        if(k!=n)l+=n-2*k+1;
    }
    cout<<l;
    
}