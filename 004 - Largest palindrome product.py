def is_palindrome(number):
    a=number
    result=0
    while(a>0):
        result=result*10+a%10
        a=a//10

    if number==result:
        return True
    else:
        return False

largest=1
i=999
while(i>99):
    j=i
    while j>99:
        if is_palindrome(j*i):
            if j*i>largest:
                largest=j*i
        j=j-1
    i=i-1
print (largest)
