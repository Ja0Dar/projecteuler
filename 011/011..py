def find_horizontal():
    global tab,largest,biggest
    for y in range(0,20):
        x=0
        for x in range(0,17) :
            iloczyn=tab[y][x]*tab[y][x+1]*tab[y][x+2]*tab[y][x+3]
            if iloczyn>largest:
                largest= iloczyn
                biggest=(x,y)
def find_vertical():
    global tab,largest,biggest
    for x in range(0,20):
        for y in range(0,17):
            iloczyn=tab[y][x]*tab[y+1][x]*tab[y+2][x]*tab[y+3][x]
            if iloczyn>largest:
                largest=iloczyn
                biggest=(x,y)
def find_diagonal_left_right():
    global tab,largest,biggest
    for x in range(0,17):
        for y in range(0,17):
            iloczyn=tab[y][x]*tab[y+1][x+1]*tab[y+2][x+2]*tab[y+3][x+3]
            if iloczyn>largest:
                largest=iloczyn
                biggest=(x,y)
def find_diagonal_right_left():
    global tab,largest,biggest
    for x in range(3,20):
        for y in range(0,17):
            iloczyn=tab[y][x]*tab[y+1][x-1]*tab[y+2][x-2]*tab[y+3][x-3]
            if iloczyn>largest:
                largest=iloczyn
                biggest=(x,y)          

plik=open("grid.txt")
tab=[]
#tab [y].[x]
row=[]
biggest=(0,0)
for line in plik.readlines():
    row=[]
    
    for box in line.split(" "):
        row.append(int(box))
    tab.append(row)

largest=0
find_horizontal()
find_vertical()
find_diagonal_left_right()
find_diagonal_right_left()



for lines in tab:
    a=""
    for box in lines:
        if box<10:
            box="0"+str(box)
        else:
            box=str(box)
        a=a+box+"  "
    print(a)

print()
print(largest," ",biggest)
