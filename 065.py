#   https://projecteuler.net/problem=65
#
def NWD(a,b):
    while a!=0:
        tmp=a
        a=b%a
        b=tmp
    return b

class ulamek:
    l=0
    m=1
    def __init__(self, licz,mian):
        self.l=licz
        self.m=mian
    def wypisz(self):
        print("%i / %i"%(self.l,self.m))
    def skroc(self):
        nwd=NWD(self.l,self.m)
        self.l//=nwd
        self.m//=nwd
    def odwroc(self):
        tmp=self.l
        self.l=self.m
        self.m=tmp
        return self
    
def dodajUlamek(a,b):
    licznik=a.l*b.m+a.m*b.l
    mianownik=a.m*b.m
    nwd=NWD(licznik,mianownik)
    licznik//=nwd
    mianownik//=nwd
    return ulamek(licznik,mianownik)
x=ulamek(0,1)
k=2
n=100

for i in range(n,0,-1):
    if(i%3==2):
        a=2*(i//3+1)
    else:
        a=1
    x=dodajUlamek(ulamek(a,1),x.odwroc())

x=dodajUlamek(ulamek(2,1),x.odwroc())
suma=0
for i in str(x.l):
    suma+=int(i)
print(suma)
