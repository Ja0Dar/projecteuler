#include <iostream>
using namespace std;

int maxliczba;

bool is_prime(int liczba){
    if(liczba==2) return true;
    else if(liczba%2==0)return false;
    else if(liczba<2)return false;
    else{

        int factor=3;
        while(factor*factor<=liczba){
            if(liczba%factor==0)return false;
            factor=factor+2;
        }
        return true;
        
    }
}


int tabToInt(int (&tablica)[9],int dlugosc){
    int liczba=0;
    for(int i=0;i<dlugosc;i++){
        liczba=liczba*10+tablica[i];
    }
    return liczba;
}

void rotacja(int (&tablica)[9],int co, int z_czym){
    int a=tablica[co];
    tablica[co]=tablica[z_czym];
    tablica[z_czym]=a;
    
}

void perm(int(&tablica)[9],int k,int l_cyfr){
    if(k==0){
        int liczba=tabToInt(tablica,l_cyfr);
        if(is_prime(liczba)){
            cout<<liczba;
            cout<<endl;
            if(liczba>maxliczba)maxliczba=liczba;
        }
        
        
        
        
    }else{
        for(int j=0;j<k+1;j++){
            rotacja(tablica,j,k);
            perm(tablica,k-1,l_cyfr);
            rotacja(tablica,j,k);
        }
    }
}



int main(){
    int tab[9];
    maxliczba=0;
    for(int i=0;i<9;i++)tab[i]=i+1;
    //for(int i=0;i<9;i++)cout<<tab[i];
   for (int o=2;o<10;o++){
       for(int i=0;i<o;i++)tab[i]=i+1;
       perm(tab,o-1,o);
   }
   cout<<endl<<endl<<"max="<<maxliczba;
}
