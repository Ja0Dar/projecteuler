/*Consider the fraction, n/d, where n and d are positive integers. If n<d and HCF(n,d)=1, it is called 
a reduced proper fraction.

If we list the set of reduced proper fractions for d ≤ 8 in ascending order of size, we get:

1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, 2/5, 3/7, 1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, 5/6, 6/7, 7/8

It can be seen that 2/5 is the fraction immediately to the left of 3/7.

By listing the set of reduced proper fractions for d ≤ 1,000,000 in ascending order of size, 
find the numerator of the fraction immediately to the left of 3/7.
*/
#include<iostream>

using namespace std;

int NWD(int a, int b){
    int tmp;
    while(a!=0){
        tmp=a;
        a=b%a;
        b=tmp;
    }
    return b;
}

class fraction{
    int l,m;
    public:
        void init(int li,int mi){
            int nwd =NWD(li,mi);
            l=li/nwd;
            m=mi/nwd;
            
            
        }    
        bool gt(fraction b){
            return l*b.m>m*b.l;
        }
        void wypisz(){
            cout<<l<<"/"<<m<<endl;
        }
};



int main(){
    //fraction cel,closest,checked;
    //el.init(3,7);
    //nevermind
    cout<<int(1000000/7)*3-1;
    
}