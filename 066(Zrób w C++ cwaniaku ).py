from math import *

def is_square(a):
    hexDgt=a%16
    if hexDgt>9:
        return False
    if hexDgt in [0,4,1,9]:
        b=int(sqrt(a))
        return b*b==a
    return False
def NWD(a,b):
    while a!=0:
        tmp=a
        a=b%a
        b=tmp
    return b


class wyr:
    pierw=0
    mult=1
    dgt=1
    def __init__(self,pierw,dgt,mult):
        self.pierw=pierw
        self.mult=mult
        self.dgt=dgt
        
    def wypisz(self):
        print("%d(%d + %d)"%(self.mult,self.pierw,self.dgt))

class frac: #brak zabezpieczenia przed 0
    l=wyr(1,1,1)
    m=wyr(1,1,1)
    def __init__(self,licznik,mianownik):
        self.l=licznik
        self.m=mianownik
    def wypisz(self):
        self.l.wypisz()
        print("-----------")
        self.m.wypisz()
        print(" ")
    def odwroc(self):
        tmp=self.l
        self.l=self.m
        self.m=tmp
    def uniewymiernij(self):
        if self.m.pierw!=0 and self.l.pierw==0 :
            x=self.m.pierw
            tmp=self.m.dgt
            self.m.dgt=x-tmp*tmp;
            self.l.mult=self.l.dgt;
            nwd=NWD(self.l.mult,self.m.dgt);
            self.l.mult//=nwd;
            self.m.dgt//=nwd;
            self.l.pierw=x;
            self.m.pierw=0;
            self.m.mult=1;
            self.l.dgt=tmp*-1;
        else:
            print("uniewymierniasz bez niewymiernosci")
    def wylaczPrzedUlamek(self):
        a0=int(sqrt(self.l.pierw))
        wynik=(a0+self.l.dgt)//self.m.dgt
        self.l.dgt=self.l.dgt-wynik*self.m.dgt
        return wynik


    def kopiuj(self):
        return frac(wyr(self.l.pierw,self.l.dgt,self.l.mult),wyr(self.m.pierw,self.m.dgt,self.m.mult))
                    
def rowneWyr(a,b):
    return a.pierw==b.pierw and a.mult==b.mult and a.dgt==b.dgt
def rowneFrac(a,b):
    return rowneWyr(a.l,b.l) and rowneWyr(a.m,b.m)
def uposledzoneDodajFrac(a,b):
    c=frac(wyr(0,a.l.dgt*b.m.dgt+b.l.dgt*a.m.dgt,1),wyr(0,a.m.dgt*b.m.dgt,1))
    nwd=NWD(c.l.dgt,c.m.dgt)
    c.m.dgt//=nwd
    c.l.dgt//=nwd
    return c

def rozwin(x):
    wynik=[]
    wynik.append(int(sqrt(x)))
    if wynik[0]*wynik[0]!=x:
        a=frac(wyr(x,-1*wynik[0],1),wyr(0,1,1))
        reszta=a.kopiuj()


        a.odwroc()
        a.uniewymiernij()
        wynik.append(a.wylaczPrzedUlamek())
        while not rowneFrac(a,reszta):
            a.odwroc()
            a.uniewymiernij()
            wynik.append(a.wylaczPrzedUlamek())
        
        
        
    return wynik
            
       
    
def zwin(roz,depth,i,maxdepth):
    if depth==maxdepth:
        return frac(wyr(0,roz[i],1),wyr(0,1,1))
    wartoscRozwiniecia=roz[i]
    i+=1
    i=i%len(roz)
    if i==0:
        i+=1
    c=zwin(roz,depth+1,i,maxdepth)
    tmp=c.l
    c.l=c.m
    c.m=tmp
    nr=frac(wyr(0,wartoscRozwiniecia,1),wyr(0,1,1))
    c=uposledzoneDodajFrac(nr,c)
    return c

maxD=0
maxX=0
for D in range(2,1000):
    if not is_square(D):
        f=rozwin(D)
        i=1
        c=zwin(f,0,0,i)
        i+=1
        while (c.l.dgt*c.l.dgt-D*c.m.dgt*c.m.dgt)!=1:
            c=zwin(f,0,0,i)
            i+=1
        if c.l.dgt>maxX:
            maxX=c.l.dgt
            maxD=D
print(maxD)
        
            
    
