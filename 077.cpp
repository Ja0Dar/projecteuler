#include <iostream>
using namespace std;
const int N=71;

bool inPrimes(int sz,int t[]);

int podzialy(int l,int odj/*ostatni odjety prime*/,int primes[N],int podzial[N][N]){
    
    if(podzial[l][odj]==-1){/// Gdy jest x=3 lub x=2 to podwojnie zlicza chyba 
        int x;
        int suma=0;
        
        //Jesli liczba przekazana w l jest licz. pierwsza, mniejza od poprzednio odjetych
        if(inPrimes(l,primes) and l<=primes[odj])suma++;
        
        //przesuwam odj tak, aby od l odejmowac l pierwsze mniejsze od nie
        while(odj>=0 and primes[odj]>=l)odj--;
        //jesli l< primes[odj] 
        if(odj<0){
            //cout<<endl<<l<<" "<<suma;
            return suma;
        }
        //Musi byc dla tego ze 10=5+2+3 nie moze byc przyjmowane.. nie ogarniam tego tak do konca
        if(l<4) return suma;
        for(int i=odj;i>=0;i--){
            x=l-primes[i];
            suma+=podzialy(x,i,primes,podzial);
        }
        podzial[l][odj]=suma;
    }
    return podzial[l][odj];
}

void napelnijPrimes(int p[N]){
    int sN=15*N;
    bool sieve[sN];
    for(int i=2;i<sN;i++)sieve[i]=true;
    sieve[0]=sieve[1]=false;
    int k=2;
    while(k<sN){
        while(!sieve[k] and k<sN)k++;
        for(int i=k+k;i<sN;i+=k)sieve[i]=false;//to powinno zatrzymac sie dla k>sN
        k++;
    }
    int licz=0;
    k=0;
    while(licz<N){
        while(!sieve[k])k++;
        p[licz]=k;
        licz++;
        k++;
    }
}
bool inPrimes(int sz,int t[]){
    int l=0;
    int p=N;
    int sr;
    while(l<=p){
        sr=(l+p)/2;
        if(sz<t[sr])p=sr-1;
        else l=sr+1;
    }
    if(t[p]==sz)return true;
    return false;
    
    
}
int posInPrimes(int sz,int t[]){
    int l=0;
    int p=N;
    int sr;
    while(l<=p){
        sr=(l+p)/2;
        if(sz<t[sr])p=sr-1;
        else l=sr+1;
    }
    //if(t[p]==sz)return p;
    //return -1;
    return p;
    
    
}

int main(int argc, char* argv[]){
    int podzial[N][N];
    int primes[N];
    
    
    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
            podzial[i][j]=-1;
        }
    }
    napelnijPrimes(primes);
    int prI=posInPrimes(N,primes);//pozycja najwiekszej prime mniejszej od N w primes 
    cout<<podzialy(N-1,prI,primes,podzial);
    
    //cout<<podzialy(3,4,primes,podzial);
}

